package simulator;

import command.model.Assenseur;
import command.model.Etat;
import org.junit.jupiter.api.Test;
import java.util.concurrent.TimeUnit;
import static org.junit.jupiter.api.Assertions.*;

class SimulatorRunnableTest {

    @Test
    void testSimulatorStop() throws InterruptedException {
        Assenseur model = new Assenseur();
        SimulatorRunnable simulator = new SimulatorRunnable(1, model);

        Thread thread = new Thread(simulator);
        thread.start();

        // Laisser le simulateur s'exécuter pendant 500 ms
        TimeUnit.MILLISECONDS.sleep(500);

        simulator.stop();
        thread.join();

        assertFalse(simulator.isRunning());
    }

    @Test
    void testSimulatorMovingUp() throws InterruptedException {
        Assenseur model = new Assenseur();
        SimulatorRunnable simulator = new SimulatorRunnable(1, model);

        Thread thread = new Thread(simulator);
        thread.start();

        // Laisser le simulateur s'exécuter pendant 500 ms
        TimeUnit.MILLISECONDS.sleep(500);

        // Vérifier que la position augmente dans l'état MOVING_UP
        assertTrue(model.getPosition() > 0);
        assertEquals(Etat.MOVING_UP, model.getState());

        simulator.stop();
        thread.join();
    }

    @Test
    void testSimulatorMovingDown() throws InterruptedException {
        Assenseur model = new Assenseur();
        model.setPosition(model.nbFloor); // positionner à l'étage le plus haut
        model.setState(Etat.MOVING_DOWN); // définir l'état MOVING_DOWN

        SimulatorRunnable simulator = new SimulatorRunnable(1, model);

        Thread thread = new Thread(simulator);
        thread.start();

        // Laisser le simulateur s'exécuter pendant 500 ms
        TimeUnit.MILLISECONDS.sleep(500);

        // Vérifier que la position diminue dans l'état MOVING_DOWN
        assertTrue(model.getPosition() < model.nbFloor);
        assertEquals(Etat.MOVING_DOWN, model.getState());

        simulator.stop();
        thread.join();
    }

    // Ajouter d'autres tests en fonction de la logique spécifique de votre application
}
