package command.model;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AssenseurTest {

    @Test
    void testInitialState() {
        Assenseur assenseur = new Assenseur();
        assertEquals(Etat.STOPPED, assenseur.getState());
        assertEquals(0, assenseur.getPosition());
        assertEquals(0, assenseur.getPersonne());
    }

    @Test
    void testSetState() {
        Assenseur assenseur = new Assenseur();
        assenseur.setState(Etat.MOVING_UP);
        assertEquals(Etat.MOVING_UP, assenseur.getState());
    }

    @Test
    void testSetPosition() {
        Assenseur assenseur = new Assenseur();
        assenseur.setPosition(5.0);
        assertEquals(5.0, assenseur.getPosition());
    }

    @Test
    void testSetAddPersonne() {
        Assenseur assenseur = new Assenseur();
        assenseur.setAddPersonne();
        assertTrue(assenseur.getPersonne() > 0);
    }

    @Test
    void testSetRmPersonne() {
        Assenseur assenseur = new Assenseur();
        assenseur.setAddPersonne();
        int initialPersonne = assenseur.getPersonne();
        assenseur.setRmPersonne();
        assertTrue(assenseur.getPersonne() < initialPersonne);
    }

    @Test
    void testSetRemovePersonne() {
        Assenseur assenseur = new Assenseur();
        assenseur.setAddPersonne();
        int initialPersonne = assenseur.getPersonne();
        assenseur.setRemovePersonne(2);
        assertEquals(initialPersonne - 2, assenseur.getPersonne());
    }
}
