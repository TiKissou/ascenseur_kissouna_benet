package command.model;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class EtatTest {

    @Test
    public void testEnumValues() {
        Etat[] etats = Etat.values();

        assertEquals(6, etats.length);

        assertEquals(Etat.STOPPED, etats[0]);
        assertEquals(Etat.EMERGENCY, etats[1]);
        assertEquals(Etat.MOVING_UP, etats[2]);
        assertEquals(Etat.MOVING_DOWN, etats[3]);
        assertEquals(Etat.MOVING_UP_STOP_NEXT, etats[4]);
        assertEquals(Etat.MOVING_DOWN_STOP_NEXT, etats[5]);
    }

    @Test
    public void testEnumToString() {
        assertEquals("STOPPED", Etat.STOPPED.toString());
        assertEquals("EMERGENCY", Etat.EMERGENCY.toString());
        assertEquals("MOVING_UP", Etat.MOVING_UP.toString());
        assertEquals("MOVING_DOWN", Etat.MOVING_DOWN.toString());
        assertEquals("MOVING_UP_STOP_NEXT", Etat.MOVING_UP_STOP_NEXT.toString());
        assertEquals("MOVING_DOWN_STOP_NEXT", Etat.MOVING_DOWN_STOP_NEXT.toString());
    }


}