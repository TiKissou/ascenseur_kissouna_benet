package command;

import command.model.Assenseur;
import command.request.Request;
import command.request.RequestType;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

public class ShortestStrategyTest {

    @Test
    public void testChooseFloor() {
        // Créez une instance de la classe Assenseur (vous pouvez ajuster les valeurs au besoin)
        Assenseur model = new Assenseur();
        model.setPosition(3);

        // Créez une instance de ShortestStrategy
        ShortestStrategy strategy = new ShortestStrategy(model);

        // Créez une liste de requêtes pour tester
        List<Request> listRequest = new ArrayList<>();
        listRequest.add(new Request(RequestType.GO_TO, 5));
        listRequest.add(new Request(RequestType.GO_TO, 2));
        listRequest.add(new Request(RequestType.OUTSIDE_UP, 6));
        listRequest.add(new Request(RequestType.OUTSIDE_DOWN, 1));

        // Testez la méthode chooseFloor
        Request chosenRequest = strategy.chooseFloor(listRequest);

        // Vérifiez que la requête choisie est celle attendue
        assertNotNull(chosenRequest);
        assertEquals(RequestType.GO_TO, chosenRequest.getRequestType());
        assertEquals(2, chosenRequest.getPosition());
    }

    @Test
    public void testNextRequest() {
        // Créez une instance de la classe Assenseur (vous pouvez ajuster les valeurs au besoin)
        Assenseur model = new Assenseur();
        model.setPosition(3);

        // Créez une instance de ShortestStrategy
        ShortestStrategy strategy = new ShortestStrategy(model);

        // Créez une liste de requêtes pour tester
        List<Request> listRequest = new ArrayList<>();
        listRequest.add(new Request(RequestType.GO_TO, 5));
        listRequest.add(new Request(RequestType.OUTSIDE_UP, 6));

        // Testez la méthode nextRequest
        Request nextRequest = strategy.nextRequest(new ArrayList<>(listRequest));

        // Vérifiez que la requête suivante est celle attendue
        assertNotNull(nextRequest);
        assertEquals(RequestType.GO_TO, nextRequest.getRequestType());
        assertEquals(5, nextRequest.getPosition());
    }

    @Test
    public void testGetListOfAction() {
        // Créez une instance de la classe Assenseur (vous pouvez ajuster les valeurs au besoin)
        Assenseur model = new Assenseur();
        model.setPosition(3);

        // Créez une instance de ShortestStrategy
        ShortestStrategy strategy = new ShortestStrategy(model);

        // Créez une liste de requêtes pour tester
        List<Request> listRequest = new ArrayList<>();
        listRequest.add(new Request(RequestType.GO_TO, 5));
        listRequest.add(new Request(RequestType.OUTSIDE_UP, 6));
        listRequest.add(new Request(RequestType.GO_TO, 2));
        listRequest.add(new Request(RequestType.OUTSIDE_DOWN, 1));

        // Créez une requête actuelle pour tester
        Request currentRequest = new Request(RequestType.GO_TO, 4);

        // Testez la méthode getListOfAction
        ArrayList<Request> listOfAction = strategy.getListOfAction(new ArrayList<>(listRequest), currentRequest, RequestType.OUTSIDE_UP);

        // Vérifiez que la liste d'actions est celle attendue
        assertNotNull(listOfAction);
        assertEquals(0, listOfAction.size());
    }

}
