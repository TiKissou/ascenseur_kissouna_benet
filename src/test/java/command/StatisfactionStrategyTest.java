package command;

import command.request.Request;
import command.request.RequestType;

import java.util.ArrayList;

public class StatisfactionStrategyTest implements SatisfactionStrategy {

    @Override
    public Request nextRequest(ArrayList<Request> listRequest) {
        if (listRequest.isEmpty()) {
            return null; // Aucune requête en attente
        }

        // Retourne la première requête de la liste pour simplifier le test
        return listRequest.get(0);
    }

    @Override
    public ArrayList<Request> getListOfAction(ArrayList<Request> listRequest, Request currentRequest, RequestType requestType) {
        // Simule une stratégie qui retourne toutes les requêtes de la liste
        ArrayList<Request> actions = new ArrayList<>(listRequest);
        return actions;
    }
}
