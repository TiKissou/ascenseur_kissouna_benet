package command;

import command.model.Etat;
import command.request.Request;
import command.request.RequestType;
import simulator.AssenseurSimulator;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CommandTest {

    @Test
    public void testAddRequest() {
        AssenseurSimulator simulator = new AssenseurSimulator(6, 2);
        Command command = new Command(simulator, new ShortestStrategy(simulator.getModel()));

        Request request1 = new Request(RequestType.GO_TO, 2);
        Request request2 = new Request(RequestType.OUTSIDE_UP, 3);
        Request request3 = new Request(RequestType.OUTSIDE_DOWN, 1);
        Request request4 = new Request(RequestType.URGENCY, -1);
        Request nullRequest = null; // Nouveau cas : requête nulle

        // Cas de base : Ajouter la première requête
        command.addRequest(request1);
        assertEquals(request1, command.getCurrentRequest());

        // Cas : Ajouter une requête lorsque la liste n'est pas vide
        command.addRequest(request2);
        assertEquals(request1, command.getCurrentRequest());
        assertTrue(command.getListRequest().contains(request2));

        // Cas : Ajouter une requête urgente
        command.addRequest(request4);
        assertEquals(Etat.STOPPED, command.getStateInEmergency());

        // Cas : Ajouter une requête lorsque la liste est vide
        command.getListRequest().clear(); // Nettoyer la liste pour ce test
        command.addRequest(request3);
        assertEquals(request1, command.getCurrentRequest());
        assertFalse(command.getListRequest().isEmpty());

        // Cas : Ajouter une requête nulle (ne devrait rien changer)
        command.addRequest(nullRequest);
        assertEquals(request1, command.getCurrentRequest());
        assertFalse(command.getListRequest().isEmpty());
    }

    @Test
    public void testRequestProcessing() throws InterruptedException {
        AssenseurSimulator simulator = new AssenseurSimulator(6, 2);
        Command command = new Command(simulator, new ShortestStrategy(simulator.getModel()));
        command.start();

        Request request1 = new Request(RequestType.GO_TO, 2);
        Request request2 = new Request(RequestType.OUTSIDE_UP, 3);

        command.addRequest(request1);
        Thread.sleep(1000); 
        assertEquals(Etat.MOVING_UP, simulator.getModel().getState());
        assertEquals(request1, command.getCurrentRequest());

        command.addRequest(request2);
        Thread.sleep(1000);
        assertEquals(Etat.MOVING_UP, simulator.getModel().getState());
        assertEquals(request1, command.getCurrentRequest());

        command.stop();
        assertEquals(Etat.MOVING_UP, simulator.getModel().getState());
    }
}
