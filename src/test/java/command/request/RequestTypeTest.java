package command.request;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class RequestTypeTest {

    @Test
    public void testEnumValues() {
        RequestType[] types = RequestType.values();

        assertEquals(5, types.length);

        assertEquals(RequestType.GO_TO, types[0]);
        assertEquals(RequestType.OUTSIDE_UP, types[1]);
        assertEquals(RequestType.OUTSIDE_DOWN, types[2]);
        assertEquals(RequestType.STOP_URGENCY, types[3]);
        assertEquals(RequestType.URGENCY, types[4]);
    }

    @Test
    public void testEnumToString() {
        assertEquals("GO_TO", RequestType.GO_TO.toString());
        assertEquals("OUTSIDE_UP", RequestType.OUTSIDE_UP.toString());
        assertEquals("OUTSIDE_DOWN", RequestType.OUTSIDE_DOWN.toString());
        assertEquals("STOP_URGENCY", RequestType.STOP_URGENCY.toString());
        assertEquals("URGENCY", RequestType.URGENCY.toString());
    }

}
