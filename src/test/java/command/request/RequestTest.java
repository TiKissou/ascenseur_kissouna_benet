package command.request;

import org.junit.jupiter.api.Test;
import java.util.Comparator;
import static org.junit.jupiter.api.Assertions.*;

public class RequestTest {

    @Test
    public void testRequestConstruction() {
        Request request = new Request(RequestType.OUTSIDE_UP, 3.5);

        assertEquals(RequestType.OUTSIDE_UP, request.getRequestType());
        assertEquals(3.5, request.getPosition(), 0.0001);
    }

    @Test
    public void testToString() {
        Request request = new Request(RequestType.GO_TO, 2);

        assertEquals("Request{requestType=GO_TO, floor=2.0}", request.toString());
    }

    @Test
    public void testComparator() {
        Request request1 = new Request(RequestType.OUTSIDE_UP, 4);
        Request request2 = new Request(RequestType.OUTSIDE_DOWN, 2);

        Comparator<Request> comparator = Request.RequestComparator;

        assertEquals(-2, comparator.compare(request1, request2));
        assertEquals(2, comparator.compare(request2, request1)); 
        assertEquals(0, comparator.compare(request1, request1));
    }

    @Test
    public void testComparable() {
        Request request1 = new Request(RequestType.GO_TO, 4);
        Request request2 = new Request(RequestType.GO_TO, 2);

        assertEquals(2, request1.compareTo(request2));
        assertEquals(-2, request2.compareTo(request1)); 
        assertEquals(0, request1.compareTo(request1));
    }


}
