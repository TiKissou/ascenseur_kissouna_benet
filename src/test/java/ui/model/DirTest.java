package ui.model;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class DirTest {

    @Test
    void testEnumValues() {
        // Vérifiez que les valeurs de l'énumération sont correctes
        assertEquals(Dir.values().length, 2);
        assertEquals(Dir.UP, Dir.valueOf("UP"));
        assertEquals(Dir.DOWN, Dir.valueOf("DOWN"));
    }

    @Test
    void testToString() {
        // Vérifiez que la méthode toString renvoie les bonnes chaînes
        assertEquals("UP", Dir.UP.toString());
        assertEquals("DOWN", Dir.DOWN.toString());
    }

    @Test
    void testEnumEquality() {
        // Vérifiez l'égalité des instances de l'énumération
        assertSame(Dir.UP, Dir.UP);
        assertSame(Dir.DOWN, Dir.DOWN);
        assertNotSame(Dir.UP, Dir.DOWN);
    }
}
