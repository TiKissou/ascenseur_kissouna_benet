package ui.view.component;

import command.model.Etat;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.swing.*;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ElevatorStatePanelTest {

    private ElevatorStatePanel statePanel;

    @BeforeEach
    void setUp() {
        statePanel = new ElevatorStatePanel();
    }

    @Test
    void testSetState() throws NoSuchFieldException, IllegalAccessException {
        // Utiliser la réflexion pour accéder à l'attribut privé
        Field stateLblField = ElevatorStatePanel.class.getDeclaredField("stateLbl");
        stateLblField.setAccessible(true);
        JLabel stateLbl = (JLabel) stateLblField.get(statePanel);

        // Vérifier l'état initial
        assertEquals("Porte fermée", stateLbl.getText());

        // Changer l'état à EMERGENCY
        statePanel.setState(Etat.EMERGENCY);
        assertEquals("En arrêt d'urgence", stateLbl.getText());

        // Changer l'état à MOVING_UP
        statePanel.setState(Etat.MOVING_UP);
        assertEquals("En monté", stateLbl.getText());

        // Changer l'état à MOVING_DOWN_STOP_NEXT
        statePanel.setState(Etat.MOVING_DOWN_STOP_NEXT);
        assertEquals("En descente avec arrêt au prochain étage", stateLbl.getText());
    }

    @Test
    void testSetStateWithInvalidState() throws NoSuchFieldException, IllegalAccessException {
        // Utiliser la réflexion pour accéder à l'attribut privé
        Field stateLblField = ElevatorStatePanel.class.getDeclaredField("stateLbl");
        stateLblField.setAccessible(true);
        JLabel stateLbl = (JLabel) stateLblField.get(statePanel);
        assertEquals("Porte fermée", stateLbl.getText());
    }

    @Test
    void testSetStateWithLongestString() throws NoSuchFieldException, IllegalAccessException {
        // Utiliser la réflexion pour accéder à l'attribut privé
        Field stateLblField = ElevatorStatePanel.class.getDeclaredField("stateLbl");
        stateLblField.setAccessible(true);
        JLabel stateLbl = (JLabel) stateLblField.get(statePanel);

        // Changer l'état à MOVING_DOWN_STOP_NEXT qui a la plus longue chaîne
        statePanel.setState(Etat.MOVING_DOWN_STOP_NEXT);
        assertEquals("En descente avec arrêt au prochain étage", stateLbl.getText());
    }
}
