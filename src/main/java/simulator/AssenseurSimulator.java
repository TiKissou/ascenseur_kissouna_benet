package simulator;

import command.model.Etat;
import command.model.Assenseur;

/**
 * Un simulateur d'ascenseur utilisable à des fin de démonstration.
 */
public class AssenseurSimulator {
    /**
     * Vitesse de l'ascenseur en seconde/étage.
     */
    private final int speed;
    /**
     * Le modèle contenant les données de ce simulateur.
     */
    private final Assenseur model;
    /**
     * Le thread exécutant le traitement de la simulation.
     */
    private Thread thread;
    /**
     * Le coeur du simulateur.
     *
     * @see SimulatorRunnable pour le fonctionnement.
     */
    private SimulatorRunnable runnable;

    public AssenseurSimulator(int nbFloor, int speed) {
        this.model = new Assenseur();
        this.model.state = Etat.STOPPED;
        this.model.nbFloor = nbFloor;
        this.speed = speed;
    }

    /**
     * Change l'état de l'ascenseur simulé.
     *
     * @param state le nouvel état
     */
    public void setState(Etat state) {
        this.model.state = state;
    }

    /**
     * @return le modèle lié à cette ascenseur {@link Assenseur}
     */
    public Assenseur getModel() {
        return this.model;
    }

    /**
     * Démarre le simulateur.
     */
    public void start() {
        runnable = new SimulatorRunnable(speed, getModel());
        thread = new Thread(runnable);
        thread.start();
    }

    /**
     * Arrête le simulateur.
     * Si au bout d'un cours délai le simulateur n'a pas quitté, son thread est interrompu.
     *
     * @throws InterruptedException en cas d'échec de l'attente
     */
    public void stop() throws InterruptedException {
        if (runnable != null && runnable.isRunning()) {
            runnable.stop();
            Thread.sleep(500);
            if (thread.isAlive()) {
                thread.interrupt();
            }
        }
    }
}
