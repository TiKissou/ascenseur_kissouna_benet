package command.model;

import utils.Observable;
import java.util.Random;

/**
 * Le modèle représentant l'état de l'ascenseur à afficher par la vue de démonstration.
 *
 * @see ui.view.DemoView
 */
public class Assenseur extends Observable<Assenseur> {
    /**
     * Le nombre d'étage de l'ascenseur.
     * Exemple si nbFloor = 9 alors on a le RDC et 9 étages
     */
    public int nbFloor;
    /**
     * L'état actuel de l'ascenseur.
     */
    public Etat state;
    /**
     * La position actuel de l'ascenseur.
     */
    public double position;
    
    //Nombre de personnes dans l'assenseur
    public int personne;

    public Assenseur() {
        position = 0;
        personne = 0;
        state = Etat.STOPPED;
    }

    /**
     * @return l'état de l'ascenseur
     */
    public Etat getState() {
        return this.state;
    }

    /**
     * Modifie l'état de l'ascenseur et notifie les observers.
     *
     * @param state le nouvelle état
     */
    public void setState(Etat state) {
        this.state = state;
        notifyObservers();
    }

    /**
     * @return la position de l'ascenseur
     */
    public double getPosition() {
        return this.position;
    }

    /**
     * Modifie le nombre de personne dans l'ascenseur et notifie les observers.
     *
     * @param personne le nouveau nombres de personnes
     */
    public void setAddPersonne() {
        int nbPersonne = new Random().nextInt(5) + 1;
        this.personne += nbPersonne;
        notifyObservers();
    }

    /**
     * Modifie le nombre de personne dans l'ascenseur et notifie les observers.
     *
     * @param personne le nouveau nombres de personnes
     */
    public void setRmPersonne() {
        if (this.personne > 0) { // Vérifie que le nombre de personnes est supérieur à zéro
            int nbPersonne = new Random().nextInt(this.personne) + 1;
            this.personne -= nbPersonne;
            notifyObservers();
        } else {
            System.out.println("L'ascenseur est déjà vide.");
        }
    }

    /**
     * Modifie le nombre de personne dans l'ascenseur et notifie les observers.
     *
     * @param personne le nouveau nombres de personnes
     */
    public void setRemovePersonne(int nbpersonne) {
        this.personne -= nbpersonne;
        notifyObservers();
    }

    /**
     * @return le nombre de personne dans l'ascenseur
     */
    public int getPersonne() {
        return this.personne;
    }

    /**
     * Modifie la position de l'ascenseur et notifie les observers.
     *
     * @param position la nouvelle position
     */
    public void setPosition(double position) {
        this.position = position;
        notifyObservers();
    }
}
